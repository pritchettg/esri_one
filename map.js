

      require([
        "esri/Map",
        //*** ADD ***//
        //"esri/views/MapView",
        "esri/views/SceneView"
      ], function(Map, SceneView) {
  
      var map = new Map({
        basemap: "topo-vector"
      });
  
      //*** ADD ***//
      //var view = new MapView({
      var view = new SceneView({
        container: "viewDiv",
        map: map,
        //*** ADD ***//
        //center: [-118.71511,34.09042],
        //zoom: 15,
        camera: {
          tilt: 65,
          position: {
            x: 148.956813,
            y: -20.3535965,
            z: 2500 // meters
          }
        }
      });
    });